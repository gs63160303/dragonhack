from django.shortcuts import render
from rest_framework import viewsets
from face_recognition.models import Image
from face_recognition.serializers import ImageSerializer
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN
from rest_framework.parsers import MultiPartParser
from rest_framework.decorators import parser_classes, api_view
from rest_framework import generics

@parser_classes((MultiPartParser,))
class ImageAdd(generics.CreateAPIView):
	serializer_class = ImageSerializer
	print("image add")

	def post(self, request, format=None):
		print(request.data)
		serializer = ImageSerializer(data=request.data)

		print(serializer.is_valid())
		if serializer.is_valid():
			print("Shranil bom")
			serializer.save()
			return Response(serializer.errors, status=HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)