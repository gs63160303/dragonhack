from imutils.video import VideoStream
import face_recognition
from imutils import paths
import argparse
import imutils
import pickle
import time
import os
import cv2

"""print("[INFO] quantifying faces...")
imagePaths = list(paths.list_images("../data/"))
print(imagePaths)
 
# initialize the list of known encodings and known names
knownEncodings = []
knownNames = []

for (i, imagePath) in enumerate(imagePaths):
	# extract the person name from the image path
	print("[INFO] processing image {}/{}".format(i + 1,len(imagePaths)))
	print(imagePath)
	name = imagePath.split(os.path.sep)[-1]
	print(name)
 
	# load the input image and convert it from BGR (OpenCV ordering)
	# to dlib ordering (RGB)
	image = cv2.imread(imagePath)
	rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

	boxes = face_recognition.face_locations(rgb,model="hog")
 
	# compute the facial embedding for the face
	encodings = face_recognition.face_encodings(rgb, boxes)
 
	# loop over the encodings
	for encoding in encodings:
		# add each encoding + name to our set of known names and
		# encodings
		knownEncodings.append(encoding)
		knownNames.append(name)
 
print(knownNames)
print(knownEncodings)

# dump the facial encodings + names to disk
print("[INFO] serializing encodings...")
data = {"encodings": knownEncodings, "names": knownNames}
f = open("encodings.pickle", "wb")
f.write(pickle.dumps(data))
f.close()
print("data written")"""


print("finished pickle")
cap = cv2.VideoCapture(0)

print("read camera")

print("[INFO] loading encodings...")
data = pickle.loads(open("encodings.pickle", "rb").read())


print()
while(True):
	# Capture frame-by-frame
	ret, frame = cap.read()

	# Our operations on the frame come here
	rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

	cv2.imshow('frame',rgb)

	# Display the resulting frame
	#cv2.imshow('frame',rgb)

	print("[INFO] recognizing faces...")
	boxes = face_recognition.face_locations(rgb, model="hog")
	encodings = face_recognition.face_encodings(rgb, boxes)

	names = []

	#if cv2.waitKey(1) & 0xFF == ord('q'):
	#	break
	for encoding in encodings:
		# attempt to match each face in the input image to our known
		# encodings
		matches = face_recognition.compare_faces(data["encodings"],
			encoding)
		name = "Unknown"
		# check to see if we have found a match
		if True in matches:
			# find the indexes of all matched faces then initialize a
			# dictionary to count the total number of times each face
			# was matched
			matchedIdxs = [i for (i, b) in enumerate(matches) if b]
			counts = {}
	 
			# loop over the matched indexes and maintain a count for
			# each recognized face face
			for i in matchedIdxs:
				name = data["names"][i]
				counts[name] = counts.get(name, 0) + 1
	 
			# determine the recognized face with the largest number of
			# votes (note: in the event of an unlikely tie Python will
			# select first entry in the dictionary)
			name = max(counts, key=counts.get)
		
		# update the list of names
		names.append(name)

	# When everything done, release the capture

	 # Display the resulting frame
	
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break
cap.release()
cv2.destroyAllWindows()