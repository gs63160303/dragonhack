from face.models import Image
from rest_framework import serializers

class ImageSerializer(serializers.ModelSerializer):
	class Meta:
		model = Image
		fields = ('id', 'image')

	def create(self, validated_data):
		return Image.objects.create(**validated_data)