from imutils.video import VideoStream
import face_recognition
from imutils import paths
import argparse
import imutils
import pickle
import time
import os
import cv2

import threading
import time

class MyThread(threading.Thread):

	def run(self):
		self.processing = False
		print("{} started!".format(self.getName()))   

	def process(self, rgb, data):
		
		if self.processing:
			return
		self.processing = True
		# Our operations on the frame come here
		

		# Display the resulting frame
		#cv2.imshow('frame',rgb)

		print("[INFO] recognizing faces...")
		boxes = face_recognition.face_locations(rgb, model="hog")
		encodings = face_recognition.face_encodings(rgb, boxes)

		names = []

		#if cv2.waitKey(1) & 0xFF == ord('q'):
		#	break
		for encoding in encodings:
			#print("lala")
			# attempt to match each face in the input image to our known
			# encodings
			matches = face_recognition.compare_faces(data["encodings"],encoding)
			name = "Unknown"
			#print(name)
			# check to see if we have found a match
			if True in matches:
				# find the indexes of all matched faces then initialize a
				# dictionary to count the total number of times each face
				# was matched
				matchedIdxs = [i for (i, b) in enumerate(matches) if b]
				counts = {}
			 
				# loop over the matched indexes and maintain a count for
				# each recognized face face
				for i in matchedIdxs:
					name = data["names"][i]
					counts[name] = counts.get(name, 0) + 1
			 
				# determine the recognized face with the largest number of
				# votes (note: in the event of an unlikely tie Python will
				# select first entry in the dictionary)
				name = max(counts, key=counts.get)
				#print(name)
				#print(counts)
				#print(matchedIdxs)
				
			# update the list of names
			names.append(name)
			# loop over the recognized faces
		for ((top, right, bottom, left), name) in zip(boxes, names):
			# draw the predicted face name on the image
			cv2.rectangle(rgb, (left, top), (right, bottom), (0, 255, 0), 2)
			y = top - 15 if top - 15 > 15 else top + 15
			cv2.putText(rgb, name, (left, y), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2)

		self.processing = False

class FaceRecognition:
	def __init__(self):
		print("I am alive")
		self.processing = False

	def do_encoding(self):
		print("[INFO] quantifying faces...")
		imagePaths = list(paths.list_images("../data/"))
		print()
		 
		# initialize the list of known encodings and known names
		knownEncodings = []
		knownNames = []

		for (i, imagePath) in enumerate(imagePaths):
			# extract the person name from the image path
			print("[INFO] processing image {}/{}".format(i + 1,len(imagePaths)))
			print(imagePath)
			name = imagePath.split(os.path.sep)[-1]
			print(name)
		 
			# load the input image and convert it from BGR (OpenCV ordering)
			# to dlib ordering (RGB)
			image = cv2.imread(imagePath)
			rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

			boxes = face_recognition.face_locations(rgb,model="hog")
		 
			# compute the facial embedding for the face
			encodings = face_recognition.face_encodings(rgb, boxes)
		 
			# loop over the encodings
			for encoding in encodings:
				# add each encoding + name to our set of known names and
				# encodings
				knownEncodings.append(encoding)
				knownNames.append(name)
		 
		print(knownNames)
		print(knownEncodings)

		# dump the facial encodings + names to disk
		print("[INFO] serializing encodings...")
		data = {"encodings": knownEncodings, "names": knownNames}
		f = open("encodings.pickle", "wb")
		f.write(pickle.dumps(data))
		f.close()
		print("data written")

	
class Main:
	def __init__(self):
		print("running face rec ....")
		self.main()

	def main(self):
		mythread = MyThread(name = "Thread-{}".format(1))  # ...Instantiate a thread and pass a unique ID to it
		mythread.start()                                   # ...Start the thread, invoke the run method
		time.sleep(.9) 
		print("read camera")
		cap = cv2.VideoCapture(0)
		print("read camera")

		print("[INFO] loading encodings...")
		data = pickle.loads(open("face/encodings.pickle", "rb").read())
		while(True):
			# Capture frame-by-frame
			ret, frame = cap.read()

			rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

			mythread.process(rgb, data)
			
			#print("showing image")
			cv2.imshow('frame', rgb)
			cv2.waitKey(0)
			newImg = cv2.resize(rgb, (100,100))
			cv2.imwrite('wanted_found.png', newImg)

			# When everything done, release the capture

			# Display the resulting frame
			
		cap.release()
		cv2.destroyAllWindows()