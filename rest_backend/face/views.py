from django.shortcuts import render
from rest_framework import viewsets
from face.models import Image
from face.serializers import ImageSerializer
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED,HTTP_200_OK,HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN
from rest_framework.parsers import MultiPartParser
from rest_framework.decorators import parser_classes, api_view
from rest_framework import generics
from rest_framework.renderers import BaseRenderer, TemplateHTMLRenderer, JSONRenderer 
import cv2
from django.http import HttpResponse
from datetime import datetime

from face.face_rec import Main
from os import remove

@parser_classes((MultiPartParser,))
class ImageAdd(generics.CreateAPIView):
    serializer_class = ImageSerializer
    print("image add")

    try:
        remove('uploaded_pictures')
    except:
        print("cannot remove")


    def post(self, request, format=None):
        print(request.data)
        serializer = ImageSerializer(data=request.data)

        print(serializer.is_valid())
        if serializer.is_valid():
            print("Shranil bom")
            serializer.save()
            Main()
            return Response(serializer.errors, status=HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

class GetData(generics.ListAPIView):
    model = Image
    queryset = ""
    def get(self, request, format=None):
        data = [
        {"lat":"46.0569", 
        "lng":"14.5058",
        "time":  datetime.timestamp(datetime.now()),
        "carId": "DF-456",
        "photoUrl":1},
        {
        "lat":"46.2397",
        "lng":"15.2677", 
        "time":  datetime.timestamp(datetime.now()),
        "carId": "CE-765",
        "photoUrl":2},
        {
        "lat":"45.8450",
        "lng":"15.4217", 
        "time":  datetime.timestamp(datetime.now()),
        "carId": "NM-846",
        "photoUrl":3}]
        response = Response(data, status=HTTP_200_OK)   
        return response    

class GetImage(generics.ListAPIView):
    model = Image
    def get(self, request, pk, format=None):
        print(pk)
        images = {
            '1': 'wanted_found.png',
            '2': 'face/wanted_fo2.jpg',
            '3': 'face/wanted_fo3.jpg'
        }
        filename = images[pk]
        print(filename)
        try:
            f = open(filename, 'rb')
            #newimg = cv2.resize(f,(100,100))
        except FileNotFoundError:
            return Response(status=HTTP_204_NO_CONTENT)
        response = HttpResponse(f, content_type="image/png")
        # response['Content-Length'] = size
        response['Content-Disposition'] = "attachment; filename=image1.png"
        return response
        
            