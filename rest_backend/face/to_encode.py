from imutils.video import VideoStream
import face_recognition
from imutils import paths
import argparse
import imutils
import pickle
import time
import os
import cv2

import threading
import time

print("[INFO] quantifying faces...")
imagePaths = list(paths.list_images("../data/"))
print()
 
# initialize the list of known encodings and known names
knownEncodings = []
knownNames = []

for (i, imagePath) in enumerate(imagePaths):
	# extract the person name from the image path
	print("[INFO] processing image {}/{}".format(i + 1,len(imagePaths)))
	print(imagePath)
	name = imagePath.split(os.path.sep)[-1]
	print(name)
 
	# load the input image and convert it from BGR (OpenCV ordering)
	# to dlib ordering (RGB)
	image = cv2.imread(imagePath)
	rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

	boxes = face_recognition.face_locations(rgb,model="hog")
 
	# compute the facial embedding for the face
	encodings = face_recognition.face_encodings(rgb, boxes)
 
	# loop over the encodings
	for encoding in encodings:
		# add each encoding + name to our set of known names and
		# encodings
		knownEncodings.append(encoding)
		knownNames.append(name)
 
print(knownNames)
print(knownEncodings)

# dump the facial encodings + names to disk
print("[INFO] serializing encodings...")
data = {"encodings": knownEncodings, "names": knownNames}
f = open("encodings.pickle", "wb")
f.write(pickle.dumps(data))
f.close()
print("data written")

"""

img = cv2.imread('wanted_fo2.jpg')
newImg = cv2.resize(img, (100,100))
cv2.imwrite('wanted_fo2.jpg', newImg)

img = cv2.imread('wanted_fo3.jpg')
newImg = cv2.resize(img, (100,100))
cv2.imwrite('wanted_fo3.jpg', newImg)

"""
