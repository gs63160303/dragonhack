from django.db import models
from django.conf import settings

def get_upload_path(instance, filename):
	print(settings.MEDIA_ROOT)
	return settings.MEDIA_ROOT

class Image(models.Model):
	image = models.ImageField(upload_to=get_upload_path)

	def save(self, *args, **kwargs):
		super(Image, self).save(*args,**kwargs)

# Create your models here.
